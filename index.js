const express = require('express');
const multer  = require('multer');
const upload = multer();

const port = 8888;

const app = express();

var knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: './data.db'
    },
    useNullAsDefault: false
});

app.use('/', express.static('public'));

app.get('/api/bookmarks', (req, res, next) => {
    knex.select().from('bookmark')
        .then((bookmarks) => {
            res.json({ bookmarks });
        })
        .catch(next);
})

app.post('/api/bookmarks', upload.array(), (req, res, next) => {
    if (req.body && req.body.url) {
        return knex.insert({ url: req.body.url }).into('bookmark')
            .then(() => {
                res.status(200).send();
            })
            .catch(next);
    }

    res.status(400).send();
})

app.delete('/api/bookmarks/:id', (req, res, next) => {
    let id;
    try {
        id = parseInt(req.params.id, 10);
    } catch(e) {
        return res.status(400).send();
    }

    knex.del().where('id', id).from('bookmark')
        .then((rowsDeleted) => {
            if (rowsDeleted === 1) {
                res.status(200).send();
            } else if (rowsDeleted === 0) {
                res.status(400).send();
            } else {
                res.status(500).send();
            }
        })
        .catch(next);
});

app.use(function(err, req, res, next) {
    if (!res.headersSent) {
        res.status(err.status || 500);
    }

    console.error(err);
});

knex.schema.createTableIfNotExists('bookmark', function (table) {
    table.increments();
    table.text('url');
    table.timestamps();
})
    .then(() => {
        app.listen(port, () => {
            console.log(`Listening on port ${port}`);
        });
    })
    .catch((e) => {
        throw e;
    });
