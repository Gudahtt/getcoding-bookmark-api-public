(function() {
    document.addEventListener('DOMContentLoaded', function() {
        console.log('DOM fully loaded and parsed');

        getBookmarks()
            .then(function (bookmarks) {
                updateBookmarks(bookmarks);
            })
            .catch(function(e) {
                throw e;
            });

        var addBookmarkForm = document.getElementById('add-bookmark');
        addBookmarkForm.addEventListener("submit", function (event) {
            event.preventDefault();

            addBookmark(addBookmarkForm)
                .then(getBookmarks)
                .then(function(bookmarks) {
                    updateBookmarks(bookmarks);
                    console.log('Bookmark added!');
                })
                .catch(function(e) {
                    throw e;
                });
        });
    });

    function getBookmarks() {
        return fetch('/api/bookmarks')
            .then(function(response) {
                if (response.status !== 200) {
                    return Promise.reject(new Error(response.statusText));
                }

                return response.json();
            })
            .then(function(json) {
                return json.bookmarks;
            });
    }

    function updateBookmarks(bookmarks) {
        var bookmarksContainer = document.getElementById('bookmarks');

        // empty bookmarks container
        bookmarksContainer.innerHTML = '';

        var bookmarksTable = document.createElement('table');
        bookmarksContainer.appendChild(bookmarksTable);

        for (var i = 0; i < bookmarks.length; i++) {
            var bookmark = document.createElement('tr');

            var idCell = document.createElement('td');
            idCell.innerHTML = bookmarks[i].id;
            idCell.setAttribute('style', 'display:none;');
            bookmark.appendChild(idCell);

            var urlCell = document.createElement('td');
            var urlAnchor = document.createElement('a');
            urlAnchor.innerHTML = bookmarks[i].url;
            urlAnchor.setAttribute('href', bookmarks[i].url);
            urlCell.appendChild(urlAnchor);
            bookmark.appendChild(urlCell);

            var deleteCell = document.createElement('td');
            var deleteButton = document.createElement('button');
            deleteButton.innerHTML = 'Delete';
            deleteButton.addEventListener('click', function() {
                var id = this.closest('tr').firstElementChild.innerHTML;
                fetch('/api/bookmarks/' + id, {
                    method: 'DELETE'
                })
                    .then(function(response) {
                        if (response.status !== 200) {
                            return Promise.reject(new Error(response.statusText));
                        }
                    })
                    .then(getBookmarks)
                    .then(function(bookmarks) {
                        updateBookmarks(bookmarks);
                    })
                    .catch(function(e) {
                        throw e;
                    });
            });
            deleteCell.appendChild(deleteButton);
            bookmark.appendChild(deleteCell);

            bookmarksTable.appendChild(bookmark);
        }
    }

    function addBookmark(form) {
        var bookmarkData = new FormData(form);

        return fetch('/api/bookmarks', {
            method: 'POST',
            body: bookmarkData
        })
            .then(function(response) {
                if (response.status !== 200) {
                    return Promise.reject(new Error(response.statusText));
                }
            });
    }
}());
